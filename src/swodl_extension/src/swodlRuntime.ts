/*---------------------------------------------------------
 * Copyright (C) Microsoft Corporation. All rights reserved.
 *--------------------------------------------------------*/

import { readFileSync } from 'fs';
import { EventEmitter } from 'events';
import { DebugProtocol } from 'vscode-debugprotocol';
import { showErrors, hideErrors, addDiagnostic, diagnosticCollection, checkSyntax } from './extension';
import { DiagnosticSeverity, commands } from 'vscode';
import { DH_UNABLE_TO_CHECK_GENERATOR } from 'constants';

var cp = require('child_process');

export interface MockBreakpoint {
	id: number;
	line: number;
	verified: boolean;
}

export class SwodlRuntime extends EventEmitter {
	private _sourceFile: string;
	public get sourceFile() {
		return this._sourceFile;
	}

	public variables: DebugProtocol.Variable[] = [];

	// the contents (= lines) of the one and only file
	private _sourceLines: string[];

	// This is the next line that will be 'executed'
	private _currentLine = 0;

	// maps from sourceFile to array of Mock breakpoints
	private _breakPoints = new Map<string, MockBreakpoint[]>();

	// since we want to send breakpoint events, we will assign an id to every event
	// so that the frontend can match events with breakpoints.
	private _breakpointId = 1;

	private _breakAddresses = new Set<string>();
	public _childProcess;
	private config;

	constructor(config) {
		super();
		this.config = config;
	}

	public addVar(name: string, value: string, type: string, ref: number = 0) {
		var temp = this.variables.find(element => element.name == name);
		if(temp){
				temp.value = value;
		} else {
			this.variables.push({
				name: name,
				//evaluateName: name,
				type: type,
				value: value,
				variablesReference: ref,
				//indexedVariables: ref
			});
		}
	}

	/**
	 * Start executing the given program.
	 */
	public start(program: string, stopOnEntry: boolean) {
		diagnosticCollection.clear();
		commands.executeCommand("editor.action.marker.prev");
        var args = ['debug', program, '--args']
        if (this.config.host && this.config.host != '') {
            args.push('--host', this.config.host);
        }
		if (this.config.mock && this.config.mock != '') {
			args.push("--mock");
			args.push(this.config.mock);
		}
		if ("inputs" in this.config){
			for (var name in this.config.inputs) {
				const value = this.config.inputs[name];
				args.push("-in");
				args.push(name+"="+value);
			}
		}
		this.loadSource(program);
		this._currentLine = -1;

		this.verifyBreakpoints(this._sourceFile);

        const iterator1 = this._breakPoints.entries();
        var v = iterator1.next().value;
        while(v != undefined){
            if (program == Object.values(v)[0]){
                for(var br in Object.values(v)[1]){
					var tempLine = Object.values(v)[1][br].line;
                    if (typeof(tempLine) == 'number') {
						args.push("-bp");
						args.push(`${tempLine + 1}`);
					}
                }
            }
            v = iterator1.next().value;
        }
        // if (stopOnEntry) {
        //     args.push('1');
		// }
        this._childProcess = cp.spawn('swodl', args);
		var runtime = this;

		this._childProcess.stdout.on("data", function (data) {
            var temp = JSON.parse(`${data}`.replace(/'/g, '"').replace(/[^{]*/, '')
                .replace(/None/g, 'null').replace(/Line:.*/g, '').replace(/Press "Enter".*/g, ''));
            if (temp != null) {
                for(var p in temp){
					if(typeof(temp[p]) == "object") {
						runtime.addVar(p, JSON.stringify(temp[p]), typeof(temp[p]), runtime.variables.length);
					} else {
						runtime.addVar(p, `${temp[p]}`, typeof(temp[p]), 0);
					}
                }
            }
            var line = /Line: (?<line>\d+)/g.exec(data);
            if(line){
                runtime._currentLine = parseInt(line.groups.line, 10) - 1;
				runtime.sendEvent('stopOnBreakpoint');
				const bps = <MockBreakpoint> {
					verified: true,
					line: runtime._currentLine,
					id: 0 };
				runtime.sendEvent('breakpointValidated', bps);
            }
        });
		var isError = false;
        this._childProcess.stderr.on("data", data => {
			isError = true;
			var errorLine = /Error on (?<line>\d+) line/g.exec(data.toString())
			if (errorLine)
				this._currentLine = parseInt(errorLine.groups.line, 10) - 1;
			this.err(data);
			// this.sendEvent('stopOnException');
			// const bps = <MockBreakpoint> {
			// 	verified: true,
			// 	line: this._currentLine,
			// 	id: 0 };
			// this.sendEvent('breakpointValidated', bps);

			this.sendEvent('pause');
			addDiagnostic(data.toString(), this._currentLine, 0, 20, DiagnosticSeverity.Error);
			//this.setBreakPoint(program, this._currentLine);
			showErrors();
        });
        this._childProcess.on("close", code => {
			if(isError == false)
				this.sendEvent('end');
			checkSyntax();
        });
        this._childProcess.on("exit", code => {
			checkSyntax();
        });
		if (stopOnEntry) {
			// we step once
			// this.step();
		} else {
			// we just start to run until we hit a breakpoint or an exception
			this.continue(false, false);
		}
	}

	/**
	 * Continue execution to the end/beginning.
	 */
	public continue(reverse = false, c=true) {
		//if(this._childProcess.stdin.writable){
		if(c){
			try {
				this._childProcess.stdin.write('\n');
			}
			catch {
				this.sendEvent('end');
				hideErrors();
			}
		}
		//this.fireEventsForLine(this._currentLine, undefined)
		// this.fireEventsForLine(this._currentLine, undefined);
		// for (let ln = this._currentLine+1; ln < this._sourceLines.length; ln++) {
		// 	if (this.fireEventsForLine(ln, undefined)) {
		// 		this._currentLine = ln;
		// 		return true;
		// 	}
		// }
		// //}
		// if(!this._childProcess.stdin.writable){
		// 	this.sendEvent('stopOnException');
		// }
		//this.run(reverse, undefined);
	}

	/**
	 * Step to the next/previous non empty line.
	 */
	//public step(program, reverse = false, event = 'stopOnStep') {
	public step() {
		if(this._childProcess.stdin.writable){
			this._childProcess.stdin.write('step\n');
			//this.fireEventsForLine(this._currentLine, event);
		}
		else{
			this.sendEvent('end');
		}
		//this.run(reverse, event);
	}

	/**
	 * Returns a fake 'stacktrace' where every 'stackframe' is a word from the current line.
	 */
	public stack(startFrame: number, endFrame: number): any {

		const words = this._sourceLines[this._currentLine].trim().split(/\s+/);

		const frames = new Array<any>();
		// every word of the current line becomes a stack frame.
		for (let i = startFrame; i < Math.min(endFrame, words.length); i++) {
			const name = words[i];	// use a word of the line as the stackframe name
			frames.push({
				index: i,
				name: `${name}(${i})`,
				file: this._sourceFile,
				line: this._currentLine
			});
		}
		return {
			frames: frames,
			count: words.length
		};
	}

	public getBreakpoints(path: string, line: number): number[] {

		const l = this._sourceLines[line];

		let sawSpace = true;
		const bps: number[] = [];
		for (let i = 0; i < l.length; i++) {
			if (l[i] !== ' ') {
				if (sawSpace) {
					bps.push(i);
					sawSpace = false;
				}
			} else {
				sawSpace = true;
			}
		}

		return bps;
	}

	/*
	 * Set breakpoint in file with given line.
	 */
	public setBreakPoint(path: string, line: number) : MockBreakpoint {
		const bp = <MockBreakpoint> { verified: false, line, id: this._breakpointId++ };
		let bps = this._breakPoints.get(path);
		if (!bps) {
			bps = new Array<MockBreakpoint>();
			this._breakPoints.set(path, bps);
		}
		bps.push(bp);

		this.verifyBreakpoints(path);

		return bp;
	}

	/*
	 * Clear breakpoint in file with given line.
	 */
	public clearBreakPoint(path: string, line: number) : MockBreakpoint | undefined {
		let bps = this._breakPoints.get(path);
		if (bps) {
			const index = bps.findIndex(bp => bp.line === line);
			if (index >= 0) {
				const bp = bps[index];
				bps.splice(index, 1);
				return bp;
			}
		}
		return undefined;
	}

	/*
	 * Clear all breakpoints for file.
	 */
	public clearBreakpoints(path: string): void {
		this._breakPoints.delete(path);
	}

	/*
	 * Set data breakpoint.
	 */
	public setDataBreakpoint(address: string): boolean {
		if (address) {
			this._breakAddresses.add(address);
			return true;
		}
		return false;
	}

	/*
	 * Clear all data breakpoints.
	 */
	public clearAllDataBreakpoints(): void {
		this._breakAddresses.clear();
	}

	// private methods

	private loadSource(file: string) {
		if (this._sourceFile !== file) {
			this._sourceFile = file;
			this._sourceLines = readFileSync(this._sourceFile).toString().split('\n');
		}
	}

	private verifyBreakpoints(path: string) : void {
		let bps = this._breakPoints.get(path);
		if (bps) {
			this.loadSource(path);
			bps.forEach(bp => {
				if (!bp.verified && bp.line < this._sourceLines.length) {
					const srcLine = this._sourceLines[bp.line].trim();

					// if a line is empty or starts with '+' we don't allow to set a breakpoint but move the breakpoint down
					if (srcLine.length === 0 || srcLine.indexOf('+') === 0) {
						bp.line++;
					}
					// if a line starts with '-' we don't allow to set a breakpoint but move the breakpoint up
					if (srcLine.indexOf('-') === 0) {
						bp.line--;
					}
					// don't set 'verified' to true if the line contains the word 'lazy'
					// in this case the breakpoint will be verified 'lazy' after hitting it once.
					if (srcLine.indexOf('lazy') < 0) {
						bp.verified = true;
						this.sendEvent('breakpointValidated', bp);
					}
				}
			});
		}
	}

	// private log(msg){
	// 	this.sendEvent('output', msg, this._sourceFile, this._currentLine, 0);
	// }
	private err(msg){
		this.sendEvent('output', msg, this._sourceFile, this._currentLine, 0, "stderr");
	}

	private sendEvent(event: string, ... args: any[]) {
		setImmediate(_ => {
			this.emit(event, ...args);
		});
	}
}
