'use strict';

import * as vscode from 'vscode';
import { WorkspaceFolder, DebugConfiguration, ProviderResult, CancellationToken } from 'vscode';
import { SwodlDebugSession } from './swodlDebug';
interface String {
    format(...replacements: string[]): string;
}

if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) {
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}

var cp = require('child_process');

const runMode: 'external' | 'server' | 'inline' = 'external';

var docs;
var consts;
var func_lines;
var declareConstStr = "const {0}_{1} = \"{0}/{1}\";\n";
var declareStr = "declare soap _auto {2}{3} @ {0}_{1};";

export var diagnosticCollection: vscode.DiagnosticCollection;

export function error(msg){
	vscode.window.showErrorMessage(msg);
}
export function showErrors(){
	vscode.commands.executeCommand("editor.action.marker.nextInFiles");
}
export function hideErrors(){
	vscode.commands.executeCommand("editor.action.marker.prev");
}
export function addDiagnostic(msg: string, line, l, r, type: vscode.DiagnosticSeverity){
	var document = vscode.window.activeTextEditor.document;
	var diags : vscode.Diagnostic[] = [];
	var diag = diagnosticCollection.get(document.uri);
	for (const key in diag) {
		diags.push(diag[key]);
	}
	try{
		diags.push({
			code: '',
			message: msg,
			range: new vscode.Range(
				new vscode.Position(line, l),
				new vscode.Position(line, r)),
			severity: type,
			source: '',
			// relatedInformation: [
			// 	new vscode.DiagnosticRelatedInformation(new vscode.Location(document.uri, new vscode.Range(new vscode.Position(1, 8), new vscode.Position(1, 9))), 'first assignment to `x`')
			// ]
		});
	}catch(error){
		console.error(error);
	}
	diagnosticCollection.set(document.uri, diags);
}

function getConstants() {
	var fileName = vscode.window.activeTextEditor?.document.fileName;
	var constProcess = cp.spawn('swodl', ["validate", fileName, "--const"]);
	constProcess.stdout.on("data", function (data) {
		consts = JSON.parse(data);
	});
}

function getFuncsLabelsLine() {
	var fileName = vscode.window.activeTextEditor?.document.fileName;
	var constProcess = cp.spawn('swodl', ["get-line-for", fileName]);
	constProcess.stdout.on("data", function (data) {
		func_lines = JSON.parse(data);
	});
}

var extensionContext;
export function activate(context: vscode.ExtensionContext) {
	let item = vscode.window.createStatusBarItem(
			vscode.StatusBarAlignment.Left,
			1
	);

	item.text = "SWoDL";
	item.show();

	extensionContext = context;
	vscode.workspace.saveAll()
	context.subscriptions.push(vscode.commands.registerCommand('extension.swodl.getProgramName', config => {
		return vscode.window.showInputBox({
			placeHolder: "Please enter the name of a SWoDL file in the workspace folder",
			value: "example.wf"
		});
	}));
	context.subscriptions.push(vscode.commands.registerCommand('extension.swodl.getHostName', config => {
		return vscode.window.showInputBox({
			placeHolder: "Please enter the name of MAM host name",
			value: "vmpc"
		});
	}));

	const provider = new MockConfigurationProvider();
	context.subscriptions.push(vscode.debug.registerDebugConfigurationProvider('swodl', provider));

	//error("Sorry that you need to use this extension.");

	let factory: vscode.DebugAdapterDescriptorFactory;
	switch (runMode) {
		case 'external': default:
			factory = new DebugAdapterExecutableFactory(provider);
			break;
		}

	context.subscriptions.push(vscode.debug.registerDebugAdapterDescriptorFactory('swodl', factory));
	if ('dispose' in factory) {
		context.subscriptions.push(factory);
	}

	diagnosticCollection = vscode.languages.createDiagnosticCollection('swodl');
	if (vscode.window.activeTextEditor) {
		updateDiagnostics(vscode.window.activeTextEditor.document);
	}
	vscode.workspace.onDidSaveTextDocument(() => {
		if (vscode.window.activeTextEditor.document.languageId == "swodl") {
			checkSyntax();
			//getDocs();
			getConstants();
			getFuncsLabelsLine();
			var childProcess = cp.spawn('swodl', ["doc"]);
			childProcess.stdout.on("data", function (data) {
				docs = JSON.parse(data);
		});}
	});
	getConstants();
	getFuncsLabelsLine();
	checkSyntax();
	getDocs();
	const swodlDocHighlight = vscode.languages.registerHoverProvider('swodl', {
		provideHover(doc, pos, token): ProviderResult<vscode.Hover> {
			const range = doc.getWordRangeAtPosition(pos);
            const word = doc.getText(range);
			return new vscode.Hover(docs[word]["doc"]);
		}
	});
	context.subscriptions.push(swodlDocHighlight);
	const swodlDefinition = vscode.languages.registerDefinitionProvider('swodl', {
		provideDefinition(doc, pos, token): ProviderResult<vscode.Location> {
			const range = doc.getWordRangeAtPosition(pos);
			const word = doc.getText(range);
			console.debug(word, func_lines[word]);
			const new_pos = new vscode.Position(func_lines[word], 0);
			return new vscode.Location(doc.uri, new_pos);
		}
	});
	context.subscriptions.push(swodlDefinition);

	let uploadCommand = vscode.commands.registerCommand("swodl.upload.to.host", () => {
		var fileName = vscode.window.activeTextEditor.document.fileName;
		vscode.window.showInformationMessage("Upload " + fileName);
	});
	context.subscriptions.push(uploadCommand);

	// let constCommand = vscode.commands.registerCommand("swodl.get.constances", () => {
	// 	getConstances();
	// });
	// context.subscriptions.push(constCommand);
}

export function getDocs(){
	var childProcess = cp.spawn('swodl', ["doc"]);
	childProcess.stdout.on("data", function (data) {
		docs = JSON.parse(data);
		var r = [];
		var names = []
		r.push(<vscode.CompletionItem>{
			kind: vscode.CompletionItemKind.Keyword,
			label: "declare",
			command: { command: 'default:type', arguments: [{ text: " " }]},
			insertText: new vscode.SnippetString("declare soap _auto"),
			documentation: new vscode.MarkdownString("Declare web method")} as never);
		for (const name in docs) {
			if(docs[name].type == 8) {
				names.push(<vscode.CompletionItem>{
					label: name,
					kind: vscode.CompletionItemKind.Reference,
					insertText: new vscode.SnippetString(name),
					command: { command: 'default:type', arguments: [{ text: "." }]}} as never);
				var interfaces = []
				for (const _interface in docs[name].interfaces) {
					interfaces.push(<vscode.CompletionItem>{
						label: _interface,
						kind: vscode.CompletionItemKind.Interface,
						command: { command: 'default:type', arguments: [{ text: "." }]}} as never);
					const _provider = vscode.languages.registerCompletionItemProvider(
						'swodl',
						{
							provideCompletionItems(document: vscode.TextDocument, position: vscode.Position) {
								const linePrefix = document.lineAt(position).text.substr(0, position.character);
								if (!linePrefix.endsWith("declare soap _auto " + name + '.' + _interface + ".")) {
									return undefined;
								} else {
									var methods = []
									for (const m in docs[name].interfaces[_interface].methods) {
										var method = "(" + docs[name].interfaces[_interface].methods[m].join(', ') + ")";
										var constVar = "";
										if (consts !== undefined &&
												"{0}_{1}".format(name, _interface) in consts === false) {
											constVar = declareConstStr.format(name, _interface, m, method);
										}
										var declConst = constVar + declareStr.format(name, _interface, m, method);
										methods.push(<vscode.CompletionItem>{
											label: m,
											kind: vscode.CompletionItemKind.Method,
											additionalTextEdits: [
												new vscode.TextEdit(
													new vscode.Range(new vscode.Position(position.line, 0), position),
													"")],
											insertText: declConst,
											command: { command: "workbench.action.files.save" }
											// range: new vscode.Range(new vscode.Position(position.line, 0), position)
										} as never);
									}
									return methods;
								}
							}
						},
						'.' // triggered whenever a '.' is being typed
					);
					extensionContext.subscriptions.push(_provider);
				}
				const _provider = vscode.languages.registerCompletionItemProvider(
					'swodl',
					{
						provideCompletionItems(document: vscode.TextDocument, position: vscode.Position) {
							const linePrefix = document.lineAt(position).text.substr(0, position.character);
							if (!linePrefix.endsWith("declare soap _auto " + name + ".")) {
								return undefined;
							}
							return interfaces;
						}
					},
					'.' // triggered whenever a '.' is being typed
				);
				extensionContext.subscriptions.push(_provider);
				const _provider2 = vscode.languages.registerCompletionItemProvider(
					'swodl',
					{
						provideCompletionItems(document: vscode.TextDocument, position: vscode.Position) {
							const linePrefix = document.lineAt(position).text.substr(0, position.character);
							if (!linePrefix.endsWith("declare soap _auto ")) {
								return undefined;
							}
							return names;
						}
					},
					' '
				);
				extensionContext.subscriptions.push(_provider2);
			}
		}
		const service_provider = vscode.languages.registerCompletionItemProvider('swodl', {
			provideCompletionItems(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken, context: vscode.CompletionContext) {

			return r;
		}});
		extensionContext.subscriptions.push(service_provider);
		///--------------------------
		var snippets = []
		for (const name in docs) {
			if (docs[name].type == 8){
				console.log(name);
			}
			if (docs[name].type != 8) {
				snippets.push(<vscode.CompletionItem>{
					kind: docs[name].type,
					label: name,
					insertText: new vscode.SnippetString(docs[name].snippet),
					documentation: new vscode.MarkdownString(docs[name].doc)});
			}
		}
		const swodlProvider = vscode.languages.registerCompletionItemProvider('swodl', {
			provideCompletionItems(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken, context: vscode.CompletionContext) {

				// a completion item that can be accepted by a commit character,
				// the `commitCharacters`-property is set which means that the completion will
				// be inserted and then the character will be typed.
				const commitCharacterCompletion = new vscode.CompletionItem('console');
				commitCharacterCompletion.commitCharacters = ['.'];
				commitCharacterCompletion.documentation = new vscode.MarkdownString('Press `.` to get `console.`');
				snippets.push(commitCharacterCompletion);
				// a completion item that retriggers IntelliSense when being accepted,
				// the `command`-property is set which the editor will execute after
				// completion has been inserted. Also, the `insertText` is set so that
				// a space is inserted after `new`
				const commandCompletion = new vscode.CompletionItem('new');
				commandCompletion.kind = vscode.CompletionItemKind.Keyword;
				commandCompletion.insertText = 'new ';
				commandCompletion.command = { command: 'editor.action.triggerSuggest', title: 'Re-trigger completions...' };
				snippets.push(commandCompletion);
				return snippets;
			}});
			const provider2 = vscode.languages.registerCompletionItemProvider(
				'swodl',
				{
					provideCompletionItems(document: vscode.TextDocument, position: vscode.Position) {

						// get all text until the `position` and check if it reads `console.`
						// and if so then complete if `log`, `warn`, and `error`
						const linePrefix = document.lineAt(position).text.substr(0, position.character);
						if (!linePrefix.endsWith('console.')) {
							return undefined;
						}

						return [
							new vscode.CompletionItem('log', vscode.CompletionItemKind.Method),
							new vscode.CompletionItem('warn', vscode.CompletionItemKind.Method),
							new vscode.CompletionItem('error', vscode.CompletionItemKind.Method),
						];
					}
				},
				'.' // triggered whenever a '.' is being typed
			);
		extensionContext.subscriptions.push(swodlProvider, provider2);
	});
	childProcess.on("close", code => {
		console.debug("Close: " + code.toString());
	});
	childProcess.stderr.on("data", data => {
		console.debug("Error: " + data.toString());
	});
}

export function checkSyntax(){
	diagnosticCollection.clear();
	var fileName = vscode.window.activeTextEditor.document.fileName;
	var childProcess = cp.spawn('swodl', ["validate", fileName]);
	childProcess.stdout.on("data", function (data) {
		var lines = data.toString().split("\n");
		for(var l in lines){
			var line = /Line: (?<line>\d+)\. Error: (?<error>.*)/g.exec(lines[l]);
			if(line){
				addDiagnostic(line.groups.error,
					parseInt(line.groups.line, 10) - 1,
					0, 100, vscode.DiagnosticSeverity.Error);
			}
		}
	});
	childProcess.on("close", code => {
		// console.debug(code.toString());
	});
	childProcess.stderr.on("data", data => {
		console.debug(data.toString());
	});
}

function updateDiagnostics(document: vscode.TextDocument): void {
	if (document) {
		// diagnosticCollection.set(document.uri, [{
		// 	code: '',
		// 	message: 'cannot assign twice to immutable variable `x`',
		// 	range: new vscode.Range(new vscode.Position(3, 4), new vscode.Position(3, 10)),
		// 	severity: vscode.DiagnosticSeverity.Error,
		// 	source: '',
		// 	relatedInformation: [
		// 		new vscode.DiagnosticRelatedInformation(new vscode.Location(document.uri, new vscode.Range(new vscode.Position(1, 8), new vscode.Position(1, 9))), 'first assignment to `x`')
		// 	]
		// }]);
	} else {
		diagnosticCollection.clear();
	}
}

export function deactivate() {
	// nothing to do
}


class MockConfigurationProvider implements vscode.DebugConfigurationProvider {

	/**
	 * Massage a debug configuration just before a debug session is being launched,
	 * e.g. add all missing attributes to the debug configuration.
	 */
	resolveDebugConfiguration(folder: WorkspaceFolder | undefined, config: DebugConfiguration, token?: CancellationToken): ProviderResult<DebugConfiguration> {

		// if launch.json is missing or empty
		const editor = vscode.window.activeTextEditor;
		if (!config.type && !config.request && !config.name) {
			if (editor && editor.document.languageId === 'swodl') {
				config.type = 'swodl';
				config.name = 'Launch';
				config.request = 'launch';
				config.program = '${file}';
				config.stopOnEntry = true;
			}
		}
		//editor.setDecorations(vscode.window.createTextEditorDecorationType())

		if (!config.program) {
			return vscode.window.showInformationMessage("Cannot find a program to debug").then(_ => {
				return undefined;	// abort launch
			});
		}
		return config;
	}
}

class DebugAdapterExecutableFactory implements vscode.DebugAdapterDescriptorFactory {
	private config;
	public constructor(provider){
		this.config = provider;
	}
	// The following use of a DebugAdapter factory shows how to control what debug adapter executable is used.
	// Since the code implements the default behavior, it is absolutely not neccessary and we show it here only for educational purpose.

	createDebugAdapterDescriptor(_session: vscode.DebugSession, executable: vscode.DebugAdapterExecutable | undefined): ProviderResult<vscode.DebugAdapterDescriptor> {
		// function sleep(seconds) {
		// 	var waitTill = new Date(new Date().getTime() + seconds * 1000);
		// 	while(waitTill > new Date()){}
		//   }
		//   sleep(1);

		// use the executable specified in the package.json if it exists or determine it based on some other information (e.g. the session)
		// if (!executable) {
		// 	const command = "absolute path to my DA executable";
		// 	const args = [
		// 		"some args",
		// 		"another arg"
		// 	];
		// 	const options = {
		// 		cwd: "working directory for executable",
		// 		env: { "VAR": "some value" }
		// 	};
		// 	executable = new vscode.DebugAdapterExecutable(command, args, options);
		// }

		// make VS Code launch the DA executable
		//return executable;
		return new vscode.DebugAdapterInlineImplementation(new SwodlDebugSession(_session.configuration));
	}
}
