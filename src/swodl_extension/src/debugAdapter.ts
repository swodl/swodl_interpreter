/*---------------------------------------------------------
 * Copyright (C) Microsoft Corporation. All rights reserved.
 *--------------------------------------------------------*/

 import { SwodlDebugSession } from './swodlDebug';

 SwodlDebugSession.run(SwodlDebugSession);
