# SWoDL

SWoDL extension for vscode.

> Please do not forget to install [swodl_interpreter](https://gitlab.com/vladku/swodl_interpreter) python library and plugins for it.

<img src="https://gitlab.com/vladku/swodl_interpreter/-/raw/master/src/swodl_extension/debug.png?inline=false">
