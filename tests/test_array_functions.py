from helper import interp


def test_array_aggregate():
    out = interp(
        """
    var numbers = array_create();
    numbers[0] = 5;
    numbers[1] = 3;
    numbers[3] = 7;
    var sum = array_aggregate(numbers, (int)_a + (int)_b);
    var product = array_aggregate(numbers, (int)_a * (int)_b);
    """
    )
    assert 15 == out['sum']
    assert 105 == out['product']
    assert (
        '<itemsOfArray><item index="0">5</item><item index="1">3</item><item index="3">7</item></itemsOfArray>'
        == str(out['numbers'])
    )


def test_array_all():
    out = interp(
        """
    var numbers = array_create();
    numbers[0] = 5;
    numbers[1] = 3;
    numbers[3] = 7;
    var allAreLessThan10 = array_all(numbers, (int)_value < 10);
    var allAreLessThan6 = array_all(numbers, (int)_value < 6);
    """
    )
    assert True == out['allAreLessThan10']
    assert False == out['allAreLessThan6']


def test_array_any():
    out = interp(
        """
    var numbers = array_create();
    numbers[0] = 5;
    numbers[1] = 3;
    numbers[3] = 7;
    var a = array_any(numbers, (int)_value < 4);
    var b = array_any(numbers, (int)_value < 2);
    """
    )
    assert True == out['a']
    assert False == out['b']


def test_array_select():
    out = interp(
        """
    var numbers = array_create();
    numbers[0] = 5;
    numbers[2] = 7;
    numbers[4] = 3;
    var squareRoots = array_select(numbers, (int)_value * (int)_value);
    """
    )
    assert [25, 49, 9] == out['squareRoots']


def test_array_where():
    out = interp(
        """
    var numbers = array_create();
    numbers[0] = 5;
    numbers[2] = 7;
    numbers[4] = 3;
    var lessThan6 = array_where(numbers, (int)_value < 6);
    """
    )
    assert [5, 3] == out['lessThan6']


def test_array_set():
    out = interp(
        """
    var numbers = array_create();
    numbers = array_set(numbers, 0, 5);
    numbers = array_set(numbers, 3, 7);
    numbers = array_set(numbers, 3, 8);
    """
    )
    assert [5, 8] == out['numbers']


def test_array_add():
    out = interp(
        """
    var numbers = array_create();
    numbers = array_add(numbers, 5);
    numbers = array_add(numbers, 7);
    """
    )
    assert 5 == out['numbers'][0]
    assert 7 == out['numbers'][1]


def test_array_get_by_index():
    out = interp(
        r"""
    var str = "<AXFRoot><MAObject type=\"QueryResult\" mdclass=\"QueryResult\"><GUID dmname=\"\"/><Meta name=\"NUMBEROFHITS\" format=\"string\">6192</Meta><Meta name=\"FIRSTHIT\" format=\"string\">5</Meta></MAObject><MAObject type=\"default\" mdclass=\"MEDIA\"><GUID dmname=\"\">3b4b62f5-a1ac-4f40-8e8e-5dd1a23d3dd1</GUID><Meta name=\"MAINTITLE\" format=\"string\">Modern Majesty</Meta><Meta name=\"ORIGINAL48HOURSTAPENUMBER\" format=\"string\">4</Meta></MAObject><MAObject type=\"default\" mdclass=\"MEDIA\"><GUID dmname=\"\">4ae6dca5-e52e-49fa-835d-45ceb9ba1ffc</GUID><Meta name=\"MAINTITLE\" format=\"string\">Modern Majesty</Meta><Meta name=\"ORIGINAL48HOURSTAPENUMBER\" format=\"string\">5</Meta></MAObject><MAObject type=\"default\" mdclass=\"MEDIA\"><GUID dmname=\"\">8173d4a5-bd28-43c7-b422-87da613f1bc8</GUID><Meta name=\"MAINTITLE\" format=\"string\">Modern Majesty</Meta><Meta name=\"ORIGINAL48HOURSTAPENUMBER\" format=\"string\">6</Meta></MAObject><MAObject type=\"default\" mdclass=\"MEDIA\"><GUID dmname=\"\">98eebab7-4025-4e2c-b1b3-e766f1f23d90</GUID><Meta name=\"MAINTITLE\" format=\"string\">Modern Majesty</Meta><Meta name=\"ORIGINAL48HOURSTAPENUMBER\" format=\"string\">7</Meta></MAObject><MAObject type=\"default\" mdclass=\"MEDIA\"><GUID dmname=\"\">c1929f51-a389-4e24-bb0b-74687f94236e</GUID><Meta name=\"MAINTITLE\" format=\"string\">Modern Majesty</Meta><Meta name=\"ORIGINAL48HOURSTAPENUMBER\" format=\"string\">8</Meta></MAObject><MAObject type=\"default\" mdclass=\"MEDIA\"><GUID dmname=\"\">76ca3ae7-e492-4567-9f22-6173ab7b0ee8</GUID><Meta name=\"MAINTITLE\" format=\"string\">Modern Majesty</Meta><Meta name=\"ORIGINAL48HOURSTAPENUMBER\" format=\"string\">9</Meta></MAObject></AXFRoot>";
    var x = xml_select_multi(str, "/*[local-name()='AXFRoot']/*[local-name()='MAObject'][@mdclass='MEDIA']");
    var i = 1;
    var obj = x[i];
    var path = "/MAObject/GUID";
    var r = xml_text(xml_select(obj, path));
    """
    )
    assert '4ae6dca5-e52e-49fa-835d-45ceb9ba1ffc' == out['r']
