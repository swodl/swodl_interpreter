from helper import interp


def test_compare():
    out = interp(
        """
    var x = compare("oir", "oir");
    var y = compare("oir", "pir");
    var z = compare("oir", "air");
    var a = compare("oir", "OiR", true);
    """
    )

    assert 0 == out['a']
    assert 0 == out['x']
    assert -1 == out['y']
    assert 1 == out['z']


def test_substr():
    out = interp(
        """
    var str1 = "An example string";
    var x = substr(str1, 5);
    var y = substr(str1, 5, 3);
    """
    )

    assert 'ample string' == out['x']
    assert 'amp' == out['y']


def test_length():
    out = interp(
        """
    var x = length("test");
    """
    )

    assert 4 == out['x']


def test_find():
    out = interp(
        """
    str = "a large string";
    pattern = "large";
    pos = find(str, pattern);
    pos2 = find(str, "doesn’t exist in str");
    pos3 = find(str, pattern, 5);
    """
    )

    assert 2 == out['pos']
    assert -1 == out['pos2']
    assert -1 == out['pos3']


def test_findreverse():
    out = interp(
        r"""
    str = "a large string";
    pattern = "a";
    var x = "\"";
    pos = findreverse(str, pattern);
    pos2 = findreverse(str, pattern, 2);
    """
    )

    assert 3 == out['pos']
    assert 0 == out['pos2']
    assert '"' == out['x']


def test_trim():
    out = interp(
        """
    pos = trim(" a string ");
    """
    )

    assert 'a string' == out['pos']


def test_replace():
    out = interp(
        """
    pos = replace("hurliburli", "url", "ping");
    """
    )

    assert 'hpingibpingi' == out['pos']


def test_multi_line_concat():
    out = interp(
        """
    pos = "hurliburli" +
            "url" +
            "ping";
    """
    )

    assert 'hurliburliurlping' == out['pos']
