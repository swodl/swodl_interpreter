import time

from lxml import etree

from swodl_interpreter.runner import Runner
from swodl_interpreter.lexer import Lexer
from swodl_interpreter.parser import Parser
from swodl_interpreter.interpreter import Interpreter
from swodl_interpreter.storage import Scope


def timeit(f):
    def timed(*args, **kw):
        ts = time.time()
        f(*args, **kw)
        te = time.time()
        print('func:%r args:[%r, %r] took: %2.4f sec' %
              (f.__name__, args, kw, te - ts))
        return te - ts

    return timed


def interp(text):
    scope = Scope()
    lexer = Lexer(text)
    parser = Parser(lexer, scope)
    i = Interpreter(parser, scope)
    steps = i.interpret()
    for step in steps:
        pass
    return i.scope.Global


def interpp(text):
    scope = Scope()
    lexer = Lexer(text)
    parser = Parser(lexer, scope)
    i = Interpreter(parser, scope)
    steps = i.interpret()
    for step in steps:
        pass
    return i


def xml_format(text):
    root = etree.fromstring(text)
    etree.indent(root, space='  ')
    return etree.tostring(root, encoding='unicode')
