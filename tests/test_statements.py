import os
import pytest

from helper import *
from swodl_interpreter.storage import Scope
from swodl_interpreter.asts.struct import SwodlError


def test_cycle():
    out = interp(
        """
    var i = 0;
    while(i < 5) {
        i += 1;
    }
    """
    )
    assert 5 == out['i']


def test_condition():
    out = interp(
        """
    var i = 0;
    if(i > 5) {
        i += 1;
    }
    else if (i == 0) {
        i = 999;
    }
    else {
        i = 111;
    }
    """
    )
    assert 999 == out['i']


def test_method_call():
    out = interp(
        """
    nop();
    l = length("test");
    """
    )
    assert 4 == out['l']


def test_label():
    out = interp(
        """
    var l = "";
    gosub test;
    l += "after";
    test:
        l += "label";
        return;
    """
    )
    assert 'labelafterlabel' == out['l']


def test_lexer_error():
    with pytest.raises(KeyError):
        out = interp(
            """
        l = \\ggg#
        """
        )


def test_quotation_mark_in_str():
    out = interp(
        """
    l = "after\\"";
    """
    )
    assert 'after\"' == out['l']


def test_status():
    out = interp(
        """
    [[Test status]]
    l = "after\\"";
    """
    )
    assert 'Test status' == out['_STATUS']


def test_do():
    out = interp(
        """
    i = 0;
    do {
        i += 1;
    } while(i < 2);
    """
    )
    assert 2 == out['i']


def test_retry():
    out = interp(
        """
    i = 0;
    retry(2){
        i += 1;
    } while(true);
    """
    )
    assert 2 == out['i']


def test_try_catch():
    out = interp(
        """
    try {
        throw "Some text";
    }
    catch {
        i = -1;
    }
    """
    )
    assert -1 == out['i']


def test_error():
    with pytest.raises(SwodlError):
        interp(
            """
            error "Some text";
            """
        )
    with pytest.raises(SwodlError):
        interp(
            """
            var _ErrorMessage = "Some text";
            error _ErrorMessage;
            """
        )


def test_exception_message():
    out = interp(
        """
    try{
        c = 1/0;
    }
    catch{}
    """
    )
    assert (
        'Error on 3 line. integer division or modulo by zero'
        == out['_ExceptionMessage']
    )


def test_assert():
    with pytest.raises(Exception):
        interp(
            """
        assert(false, "Some text");
        """
        )


def test_delay():
    @timeit
    def run():
        interp(
            """
        delay 0.01;
        """
        )

    assert 0.01 < run()


def test_struct():
    out = interp(
        """
    var test;
    test.a = "1";
    test.b.c = test.a;
    v = test.b.c;
    """
    )
    assert '1' == out['v']
    assert '1' == out['test']['a']
    assert '1' == out['test']['b']['c']
    assert '<struct><a>1</a><b><struct><c>1</c></struct></b></struct>' == str(
        out['test']
    )
    assert "{'a': '1', 'b': {'c': '1'}}" == repr(out['test'])


def test_struct_and_array():
    out = interp(
        """
    var c;
    c[0].x[1] = 1;
    c[1] = c[0].x[1];
    var test;
    test.a[0] = "1";
    test.a[2].c = 2;
    v = test.a[2].c;
    """
    )
    assert 2 == out['v']
    assert '1' == out['test']['a'][0]
    assert 1 == out['c'][1]

def test_special_symbol_slash_n():
    out = interp(r'var a = "\n";')
    assert '\n' == out['a']

def test_should_escape_special_symbols():
    with pytest.raises(Exception):
        interp('var test = "dasd\ dsa";')


def test_bin_op_with_nothing():
    with pytest.raises(Exception):
        interp('var test = "dasddsa" +;')


def test_str_never_closed():
    with pytest.raises(Exception):
        interp(
            r"""
        var slashes = "\" + ;
        var nnn = 11;
        """
        )


def test_semi_should_be_closed():
    with pytest.raises(Exception):
        interp(
            """
        var test = "a"
        var test = "b";
        """
        )


def test_simple_call_semi_should_be_closed():
    with pytest.raises(Exception):
        interp(
            """
        print("TEst")
        var t = 1;
        """
        )


def test_simple_exit():
    out = interp(
        """
    exit;
    var t = 1;
    """
    )
    assert 't' not in out


def test_bool_bin_op():
    out = interp(
        """
    var t = true && false;
    """
    )
    assert False == out['t']


def test_bin_ops():
    out = interp(
        """
    var t1 = 1 - 4;
    var t2 = 1 % 4;
    var t3 = 1 > 4;
    var t4 = 4 <= 4;
    var t5 = 5 >= 4;
    var t6 = 5 != 6;
    var t7 = 5 >> 6;
    var t8 = 5 << 6;
    var t9 = 5 ^ 6;
    var t10 = 5 & 6;
    var t11 = 5 | 6;
    var t12 = 5 || 6;
    var a = "a";
    var l = "EN";
    var m = str_lower(l) + "|" + a + "|" + a + "|" + a;
    """
    )
    assert -3 == out['t1']
    assert 1 == out['t2']
    assert False == out['t3']
    assert True == out['t4']
    assert True == out['t5']
    assert True == out['t6']
    assert 0 == out['t7']
    assert 320 == out['t8']
    assert 3 == out['t9']
    assert 4 == out['t10']
    assert 7 == out['t11']
    assert 5 == out['t12']
    assert 'en|a|a|a' == out['m']


def test_function():
    out = interp(
    """
    var t = Multiply(2, 4);
    function Multiply(a, b)
    {
        return a * b;
    }
    """
    )
    assert 8 == out['t']


def test_function_scope():
    out = interpp(
        """
    var test = 1;
    var t = Multiply(2, 4);
    var t2 = Multiply(3, 7);
    function Multiply(a, b)
    {
        var test = 100;
        var test2 = 999;
        return a * b;
    }
    """
    )
    assert 1 == out.scope.Global['test']
    assert 100 == out.scope.functions['Multiply'][0].Global['test']
    assert 8 == out.scope.Global['t']
    assert 'test2' not in out.scope.Global


def test_recurtion_function():
    out = interp(
        """
    function Power(base, power)
    {
        if (power != 0)
        {
            var res = Power(base, power-1);
            return base * res;
        }
        else
        {
            return 1;
        }
    }
    var res = Power(2, 3);
    """
    )
    assert 8 == out['res']


def test_include():
    lexer = Lexer(
        """
    include "myLib";
    var result = Multiply(pi, 5);
    """
    )
    scope = Scope()
    parser = Parser(lexer, scope)
    include_dir = os.path.join(os.getcwd(), 'tests', 'include')
    i = Interpreter(parser, scope, include_dir=include_dir)
    steps = i.interpret()
    for step in steps:
        pass
    assert 15.700000000000001 == i.scope.Global['result']


def test_multiple_assignment():
    out = interp(
        """
    var x = 3;
    var a = 1, b = 2, c = x;
    """
    )
    assert 1 == out['a']
    assert 2 == out['b']
    assert '3' == out['c']


def test_empty_script():
    out = interp('')


def test_node_not_supported():
    with pytest.raises(Exception):

        class Parser:
            def __init__(self):
                self.parse = lambda: None

        i = Interpreter(Parser())
        for _ in i.interpret():
            pass


def test_goto_to_sub():
    out = interp(
        """
    var a = 1;
    goto sub;
    a = 3;
    sub:
        a = 2;
        return;
    """
    )
    assert 2 == out['a']


def test_method_with_empty_variable():
    out = interp(
        """
    var a;
    array_size(a);
    """
    )
