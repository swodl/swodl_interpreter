from lxml import etree

from helper import interp, xml_format


def test_xmlencode():
    out = interp(
        """
    result = xmlencode("<sometag>");
    """
    )
    assert '&lt;sometag&gt;' == out['result']


def test_xmldecode():
    out = interp(
        """
    result = xmldecode("&lt;sometag&gt;");
    """
    )
    assert '<sometag>' == out['result']


def test_xml_text():
    out = interp(
        """
    xml = "<myxml>
                <example>
                    <a>1</a>
                    <b>2</b>
                </example>
                <example>
                    <a>10 &lt; 15</a>
                    <b>11</b>
                </example>
                <description>Test XML</description>
            </myxml>";
    result = xml_text(xml);
    """
    )
    assert '1210 < 1511Test XML' == out['result']


def test_xml_element():
    out = interp(
        """
    xml = "<myxml>
                <example>
                    <a>1</a>
                    <b>2</b>
                </example>
                <example>
                    <a>10 &lt; 15</a>
                    <b>11</b>
                </example>
                <description>Test XML</description>
            </myxml>";
    result = xml_element(xml, "example");
    result2 = xml_element(xml, "not");
    """
    )
    root = etree.fromstring(
        """
        <example>
            <a>1</a>
            <b>2</b>
        </example>"""
    )
    etree.indent(root, space='  ')
    assert etree.tostring(root, encoding='unicode') == out['result']
    assert '' == out['result2']


def test_xml_element_with_ns():
    out = interp(
        """
    xml = "<myxml xmlns='http://services.avid.com' xmlns:test='http://www.test.org'>
                <example>
                    <test:a>1</test:a>
                    <b>2</b>
                </example>
                <example>
                    <test:a>10</test:a>
                    <b>11</b>
                </example>
                <description>Test XML</description>
            </myxml>";
    var avid = xml_ns("http://services.avid.com");
    var elemWithNs = xml_element(xml, avid + "example");
    """
    )
    root = etree.fromstring(
        """
        <example xmlns="http://services.avid.com" xmlns:test="http://www.test.org">
            <test:a>1</test:a>
            <b>2</b>
        </example>"""
    )
    etree.indent(root, space='  ')
    assert etree.tostring(root, encoding='unicode') == out['elemWithNs']


def test_xml_elements():
    out = interp(
        """
    xml = "<myxml>
                <example>
                    <a>1</a>
                    <b>2</b>
                </example>
                <example>
                    <a>10</a>
                    <b>11</b>
                </example>
                <description>Test XML</description>
            </myxml>";
    result = xml_elements(xml);
    result2 = xml_elements(xml, "example");
    """
    )

    assert '<example>\n  <a>1</a>\n  <b>2</b>\n</example>' == out['result'][0]
    assert '<example>\n  <a>1</a>\n  <b>2</b>\n</example>' == out['result2'][0]

    assert '<example>\n  <a>10</a>\n  <b>11</b>\n</example>' == out['result'][1]
    assert '<example>\n  <a>10</a>\n  <b>11</b>\n</example>' == out['result2'][1]

    assert '<description>Test XML</description>' == out['result'][2]


def test_xml_descendants():
    out = interp(
        """
    xml = "<myxml>
                <example>
                    <a>1</a>
                    <b>2</b>
                </example>
                <example>
                    <a>10</a>
                    <b>11</b>
                </example>
                <description>Test XML</description>
            </myxml>";
    result = xml_descendants(xml);
    """
    )

    assert 8 == len(out['result'])


def test_xml_attribute():
    out = interp(
        """
    xml = "<myxml tag='value'>
                <description>Test XML</description>
            </myxml>";
    result = xml_attribute(xml, "tag");
    empty = xml_attribute(xml, "empty");
    """
    )

    assert 'value' == out['result']
    assert '' == out['empty']


def test_xml_select():
    out = interp(
        """
    xml = "<myxml xmlns='http://services.avid.com' xmlns:test='http://www.test.org'>
                <example>
                    <test:a>1</test:a>
                    <b>2</b>
                </example>
                <example>
                    <test:a>10</test:a>
                    <b>11</b>
                </example>
                <description>Test XML</description>
            </myxml>";
    var xpathNs = "/avid:myxml/avid:example/t:a";
    var selNodeNs = xml_select(xml, xpathNs,
    "avid=http://services.avid.com", "t=http://www.test.org");
    var empty = xml_select(xml, "/myxml/empty");
    """
    )

    assert (
        '<test:a xmlns:test="http://www.test.org" xmlns="http://services.avid.com">1</test:a>'
        == out['selNodeNs']
    )
    assert '' == out['empty']


def test_xml_select_multi():
    out = interp(
        """
    xml = "<myxml>
                <example>
                    <a>1</a>
                    <b>2</b>
                </example>
                <example>
                    <a>10</a>
                    <b>11</b>
                </example>
                <description>Test XML</description>
            </myxml>";
    result = xml_select_multi(xml, "/myxml/example/a");
    """
    )

    assert '<a>1</a>' == out['result'][0]
    assert '<a>10</a>' == out['result'][1]


def test_xml_new_element():
    out = interp(
        """
    var avid = xml_ns("http://services.avid.com");
    var t = xml_ns("http://www.test.org");
    var newXmlNs =
        xml_new_element(avid + "myxml",
            xml_new_element(avid + "example",
                xml_new_element(t + "a", xmlencode("1")),
                xml_new_element(avid + "b", xmlencode("2"))),
            xml_new_element("description", xmlencode("Simon & Garfunkel")));
    """
    )

    assert out['newXmlNs'] != ''


def test_xml_new_attribute():
    out = interp(
        """
    var attr = xml_new_attribute("tag", "value");
    var newElem = xml_new_element("example",
        attr,
        xml_new_attribute("performed_by", "Simon & Garfunkel"));
    """
    )

    assert '<__wfe_xml_attribute__ tag="value" />' == out['attr']
    assert (
        '<example tag="value" performed_by="Simon &amp; Garfunkel" />' == out['newElem']
    )


def test_xml_add():
    out = interp(
        """
    var xml = "<test><a>5</a></test>";
    var modifiedXml = xml_add(xml, xml_new_element("b", "abc"));
    var modifiedXml2 = xml_add(xml, xml_new_attribute("id", "1"));
    var modifiedXml3 = xml_add(xml, "<c f='7'>xyz</c>");
    var modifiedXml4 = xml_add(xml, xmlencode("3<=4"));
    """
    )

    assert xml_format('<test><a>5</a><b>abc</b></test>') == out['modifiedXml']
    assert xml_format('<test id="1"><a>5</a></test>') == out['modifiedXml2']
    assert xml_format(
        '<test><a>5</a><c f="7">xyz</c></test>') == out['modifiedXml3']
    assert xml_format('<test>3&lt;=4<a>5</a></test>') == out['modifiedXml4']


def test_xml_header():
    out = interp(
        r"""
    xml = "<?xml version=\"1.0\" encoding=\"utf-16\"?><myxml>
                <example>
                    <a>1</a>
                    <b>2</b>
                </example>
                <example>
                    <a>10 &lt; 15</a>
                    <b>11</b>
                </example>
                <description>Test XML</description>
            </myxml>";
    result = xml_element(xml, "example");
    """
    )
    root = etree.fromstring(
        """
        <example>
            <a>1</a>
            <b>2</b>
        </example>"""
    )
    etree.indent(root, space='  ')
    assert etree.tostring(root, encoding='unicode') == out['result']


def test_xml_as_struct():
    out = interp(
        r"""
    xml = "<test>
                <a>1</a>
                <b>
                    <x><a>123</a></x>
                    <x><a>456</a></x>
                </b>
            </test>";
    var a = xml.a;
    var b = xml.b[1].a;
    var c = xml.ccc;
    var d = xml.b[2];
    """
    )
    assert '1' == out['a']
    assert '456' == out['b']
    assert '' == out['c']
    assert '' == out['d']


def test_empty_xml():
    out = interp(
        """
    var empty = xml_element("", "example");
    """
    )

    assert '' == out['empty']


def test_select_xml_empty_doc():
    out = interp(
        """
    var empty = xml_select("", "//test/a");
    """
    )

    assert '' == out['empty']
