from swodl_interpreter.runner import Runner


def test_comment():
    out = Runner(
        """
    var urn = "urn:com.avid:common-object:interplay-mam:2348238472398:asset.VIDEO:23434-23F32-4234234-3243423";
    // var systemType = commonref_systemtype(urn);
    /* var systemId = commonref_systemid(urn);
    var type = commonref_type(urn);
    var id = commonref_id(urn);
    var newUrn = commonref_urn("interplay", "12345", "asset", "ABCD"); */
    """
    ).run().Global
    assert 'systemType' not in out
    assert 'type' not in out
    assert 'urn' in out
