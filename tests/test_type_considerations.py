from helper import interp


def test_string_string():
    out = interp('test = "4"+ "5";')
    assert '45' == out['test']


def test_double_string():
    out = interp('test = 4.5 + "5";')
    assert 9.5 == out['test']


def test_int_string():
    out = interp('test = 4 + "5";')
    assert 9 == out['test']


def test_int_double_string():
    out = interp('test = 8 / "4.0";')
    assert 2.0 == out['test']


def test_int_string_int_string():
    out = interp('test = "8" / "4";')
    assert 2 == out['test']


def test_double_string_int_string():
    out = interp('test = "8" / "4.0";')
    assert 2.0 == out['test']


def test_var_string_plus_var_string():
    out = interp(
        """var a = 8;
    var b = 4.0;
    var test = a + b;"""
    )
    assert '84.0' == out['test']


def test_var_string_del_var_string():
    out = interp(
        """var a = 8;
    var b = 4.0;
    var test = a / b;"""
    )
    assert 2.0 == out['test']


def test_var_string_int_plus_int():
    out = interp(
        """
    var a = 8;
    var test = a + 1;"""
    )
    assert 9 == out['test']


def test_var_string_plus_int():
    out = interp(
        """
    var a = "test";
    var test = a + 1;
    var test1 = a + 1.5;"""
    )
    assert 'test1' == out['test']
    assert 'test1.5' == out['test1']


def test_complex():
    out = interp(
        """var a = 2;
    var b = 4;
    var test = a + (3 * b);"""
    )
    assert 14 == out['test']


def test_minus_minus():
    out = interp(
        """ var a = 8;
        var b = 10 * a + 10 * 7 / 4;
        var c = a - - b;
        var d = a - - 0.14;"""
    )
    assert 105 == out['c']
    assert 8.14 == out['d']
