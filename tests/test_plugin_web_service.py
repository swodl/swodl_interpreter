import pytest
import pkg_resources

from swodl_interpreter.runner import Runner


@pytest.fixture
def register_plugin():
    distribution = pkg_resources.Distribution(__file__)
    ep = pkg_resources.EntryPoint.parse(
        'fake=swodl_interpreter.fake:WebFake', dist=distribution
    )
    distribution._ep_map = {'swodl_service_resolvers': {'fake': ep}}
    pkg_resources.working_set.add(distribution, 'swodl_service_resolvers')


def test_web_plugin(register_plugin):
    out = Runner(
        """
    declare const boolean OMA_UpdateOrderState(orderID, state, accesskey) as UpdateState;
        @ "EssenceManagerWS/EssenceManager";
    emobj = UpdateState("123", "321") @ "TEST/test";
    declare soap string GetVersion() @ "aaa/aa";
    """
    ).run().Global
    assert '123OMA_UpdateOrderState321' == out['emobj']


def test_web_include(register_plugin):
    out = Runner(
        """
    include "fake";
    """
    ).run().Global
    assert 123 == out['fake']


def test_web_error_method(register_plugin):
    from swodl_interpreter.lexer import Lexer
    from swodl_interpreter.parser import Parser
    from swodl_interpreter.interpreter import Interpreter
    from swodl_interpreter.storage import Scope

    lexer = Lexer(
        """
    declare void ERROR() @ "test/test";
    ERROR();
    """
    )
    scope = Scope()
    parser = Parser(lexer, scope)
    i = Interpreter(parser, scope)
    steps = i.interpret()
    try:
        for step in steps:
            pass
    except Exception as e:
        assert 'ERROR' == i.scope.Global['_ExceptionMessage']


def test_function_extention(register_plugin):
    out = Runner(
        """
    var o = ololo(2);
    """
    ).run().Global
    assert 'oloolo' == out['o']
