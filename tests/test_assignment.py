from helper import interp


def test_assignment():
    out = interp(
        """
    var x = "123";
    x += 4;
    x -= 4;
    x *= 4;
    x /= 4;
    x %= 4;
    x <<= 1;
    x >>= 1;
    var b = true;
    b &= false;
    b ^= true;
    b |= false;
    """
    )
    print(out)
    assert 3 == out['x']
    assert True == out['b']
