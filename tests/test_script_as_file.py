from swodl_interpreter.runner import Runner

def test_call_func_without_optional_argument():
    runner = Runner.from_file(open('./tests/test.wf', 'r'))
    actual = runner.run({'a':1, 'b':1}, 21, 22).Global
    assert '2' == actual['r']
