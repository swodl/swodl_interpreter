from helper import interp


def test_getelement():
    out = interp(
        """
    var s="value1,value2,value3";
    var len = getnumberofelements(s);
    element1 = getelement(s,0);
    element2 = getelement(s,1);
    element3 = getelement(s,2);
    var s= "value1##value2##value3";
    var len2 = getnumberofelements(s,"##");
    var s = "value1,,value2,";
    elems = getnumberofelements(s, ",", true);
    elems2 = getnumberofelements(s, ",", false);
    """
    )
    assert 3 == out['len']
    assert 3 == out['len2']
    assert 2 == out['elems']
    assert 4 == out['elems2']
    assert 'value1' == out['element1']
    assert 'value2' == out['element2']
    assert 'value3' == out['element3']


def test_random():
    out = interp(
        """
    r = random(4);
    """
    )
    assert 4 >= out['r']
