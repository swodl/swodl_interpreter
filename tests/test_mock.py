import pytest
from swodl_interpreter.doc import Doc
from swodl_interpreter.runner import Runner


class Args:
    host = None
    debug = False
    brake_point = []
    check_syntax = False
    show_code = False
    show_args = False
    mock = './tests/mock_config.py'
    include_folder = None

class ArgsJson(Args):
    mock = './tests/mock_config.json'


@pytest.mark.parametrize(
    'args',
    [Args, ArgsJson],
)
def test_basic_python_mock(args):
    runner = Runner(
        """
    declare string test(a) @ "a/b";
    var a = test("1");
    var b = test(2);
    """,
        args,
    )
    out = runner.run().Global
    assert 'One' == out['a']
    assert 123 == out['b']


def test_extend_functions_mock():
    runner = Runner(
        """
    var a = mock();
    """,
        Args(),
    )
    out = runner.run().Global
    assert 'Mock!' == out['a']

def test_get_services_mock():
    servises = Doc.get_all()
    assert 'test' in str(servises)
test_get_services_mock()


def test_get_config_mock():
    runner = Runner(
        """
    var key = config_get("profile", "section/section/key");
    var default = config_get("profile", "section/section/not_present", "default");
    """,
        Args(),
    )
    out = runner.run().Global
    assert 'CONFIGURATION!' == out['key']
    assert 'default' == out['default']


def test_a():
    runner = Runner(
        """
    var pppp = array_create();
    in in_wfVar;
    const _const = 1, _const2 = (int)2;
    var out_wfVar = "WF - Var: " + _const + in_wfVar;
    var c = 1;
    c += 2;
    var x;
    x.a[1][0] = 1;
    x.b.b = 2;
    var o = x.b.b;
    """,
        Args(),
    )
    out = runner.run(in_wfVar='test').Global
    import pprint

    pprint.pprint(out)
    assert 2 == out['_const2']
    assert 2 == out['o']
    assert 'WF - Var: 1test' == out['out_wfVar']
