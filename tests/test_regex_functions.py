from swodl_interpreter.runner import Runner


def test_regex_ismatch():
    out = (
        Runner(
            """
    b1 = regex_ismatch("abcabc", "bc.*");
    b2 = regex_ismatch("abcabc", "cb.*");
    """
        )
        .run()
        .Global
    )
    assert True == out['b1']
    assert False == out['b2']


def test_regex_find():
    out = (
        Runner(
            """
    b1 = regex_find("aBcabc", "bc.*", "i");
    b2 = regex_find("abcabc", "cb.*");
    """
        )
        .run()
        .Global
    )
    assert 1 == out['b1']
    assert -1 == out['b2']


def test_regex_replace():
    out = (
        Runner(
            r"""
    s1 = regex_replace("abacadabra", "b.", "xy");
    s2 = regex_replace("abacadabra", "b(.)", "x$1");
    s3 = regex_replace("test12342num", "(?<num>\\d+)", "-${num}-");
    var s = "02.03.2005 16:16 0 CONFIG.SYS";
    var regex = "([0-9]*)\\.([0-9]*)\\.([0-9]*)\\s*[0-9]*:[0-9]*\\s*[0-9]*\\s*([^\\s].*)";
    var repl = "$4: $3-$2-$1";
    var r = regex_replace(s, regex, repl);
    """
        )
        .run()
        .Global
    )
    assert 'axycadaxya' == out['s1']
    assert 'axacadaxra' == out['s2']
    assert 'test-12342-num' == out['s3']
    assert 'CONFIG.SYS: 2005-03-02' == out['r']
