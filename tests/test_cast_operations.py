from helper import interp


def test_bool():
    out = interp(
        """
    var x = true;
    var test_true = (bool) x;
    var test_false = (bool) 0;
    """
    )
    assert True == out['test_true']
    assert False == out['test_false']


def test_int():
    out = interp(
        """
    var x = 4;
    var y = 5;
    var test = (int) x + (int) y;
    """
    )
    assert 9 == out['test']


def test_float():
    out = interp(
        """
    var x = (float) 4 / 3;
    var y = 4 / (float) 3;
    var z = (float) 4 + "3";
    """
    )
    assert 1.3333333333333333 == out['x']
    assert 1.3333333333333333 == out['y']
    assert 7.0 == out['z']
