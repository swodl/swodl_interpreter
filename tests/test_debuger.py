import io
import pytest

from swodl_interpreter.__main__ import main


@pytest.fixture
def stdin(monkeypatch):
    monkeypatch.setattr('sys.stdin', io.StringIO('stop'))


def test_run(monkeypatch):
    monkeypatch.setattr('sys.argv', ['swodl', 'run', './tests/test.wf'])
    main(standalone_mode=False)


def test_inputs(monkeypatch):
    monkeypatch.setattr('sys.argv', ['swodl', 'run', '--input', 'test=123', './tests/test.wf'])
    main(standalone_mode=False)


def test_debug(monkeypatch, stdin):
    monkeypatch.setattr('sys.argv', ['swodl', 'debug', './tests/test.wf'])
    main(standalone_mode=False)


def test_brake_point(monkeypatch, stdin):
    monkeypatch.setattr('sys.argv', ['swodl', 'debug', '-bp', '5', '-bp', '11', './tests/test.wf'])
    main(standalone_mode=False)
