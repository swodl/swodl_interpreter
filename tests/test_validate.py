from dataclasses import dataclass
from swodl_interpreter.runner import Runner

@dataclass
class Args:
    check_syntax = True


# runner = Runner.from_file(open('./tests/PS_IMPORT_SUBTITLE_ImportIntoStrata.wf', 'r'), Args())
# for err in runner.validate():
#     print(f'Line: {err.line}. Error: {err.msg}')
# print("a")


def test_function():
    runner = Runner(
    """
    var a = 2;
    var t = Multiply2(a, 4);
    function Multiply2(a, b)
    {
        return a * b;
    }
    """,
    Args(),
    )
    actual = [x.msg for x in runner.validate()]
    assert [] == actual

def test_validate_test_file():
    runner = Runner.from_file(open('./tests/test.wf', 'r'), Args())
    # assert [] == [e.msg for e in runner.validate()]
    # TODO: FIX Validation of has wrong number of parameters. Expected 0 but 2 was given


# TODO: Add test with var str = "<AXFRoot><M"Que"v";

# test_validate_test_file()
def test_missing_semicolon():
    runner = Runner(
        """
    var b = "o\ ";
    var a = 3
    """,
        Args(),
    )
    actual = [x.msg for x in runner.validate()]
    assert [
            'Unrecognized escape sequence   on 2.',
            '; missing on 3 line.'
        ] == actual


def test_missing_array_brackets():
    runner = Runner(
        """
    var a = a[0;
    """,
        Args(),
    )
    actual = [x.msg for x in runner.validate()]
    assert ['] missing on 2 line.'] == actual


def test_not_closed_brackets():
    runner = Runner(
        """
    if (true) {
        if (true) {
            var c = 9;
    }
    """,
        Args(),
    )
    actual = [x.msg for x in runner.validate()]
    assert ["'{' on 2 line not closed"] == actual


def test_call_func_without_declaration():
    runner = Runner(
        """
    var a = func_without_declaration();
    """,
        Args(),
    )
    actual = [x.msg for x in runner.validate()]
    assert ['func_without_declaration not declared'] == actual


def test_call_func_with_not_right_number_of_parameters():
    runner = Runner(
        """
    var a = sqrt();
    array_create(1, 3);
    """,
        Args(),
    )
    actual = [x.msg for x in runner.validate()]
    assert [
        'sqrt has wrong number of parameters. Expected 1 but 0 was given',
        'array_create has wrong number of parameters. Expected 0 but 2 was given',
    ] == actual


def test_web_call_declaration():
    runner = Runner(
        """
    declare soap _auto test(1) @ "a/b";
    """,
        Args(),
    )
    actual = [x[1] for x in runner.validate()]
    assert [] == actual


def test_web_call_invalid_declaration():
    runner = Runner(
        """
    declare  @ "a/b";
    """,
        Args(),
    )
    actual = [x.msg for x in runner.validate()]
    assert ['Invalid web method declaration on 2. No method declaration found.'] == actual


def test_call_func_without_optional_argument():
    runner = Runner(
        r"""
    var a = regex_ismatch("af33", "\\d");
    """,
        Args(),
    )
    actual = [x[1] for x in runner.validate()]
    assert [] == actual
