import setuptools

with open('README.md', 'r') as fh:
    long_description = fh.read()

with open('requirements.txt', 'r') as r:
    requirements = r.readlines()

setuptools.setup(
    name='swodl_interpreter',
    version='1.0.1',
    author='Vladyslav Kurovskyi',
    description='SWoDL interpreter.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    package_dir={'': 'src'},
    packages=setuptools.find_namespace_packages(where='src'),
    py_modules=['swodl_interpreter'],
    url='https://gitlab.com/vladku/swodl_interpreter',
    # packages=setuptools.find_packages(),
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
    install_requires=requirements,
    license='MIT',
    entry_points={
        'console_scripts': ['swodl = swodl_interpreter.__main__:main'],
        'swodl_service_resolvers': ['mock = swodl_interpreter.mock:Mock'],
    },
)
